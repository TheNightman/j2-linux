#!/bin/bash

# Colors, bb
normal=$'\e[0m'; red=$'\e[31;01m'; green=$'\e[32;01m'; yellow=$'\e[33;01m'

print_status() {
	echo -n -e "${yellow}* ${green}${*}${normal}\n";
}

print_neutral() {
	echo -n -e "${yellow}* ${*}${normal} \n";
}

print_err() {
	echo -n -e "${yellow}* ${red}${*}${normal} \n";
}

# This script must be run as root!
if [ "$(id -u)" != "0" ]; then
	print_err "This script must be run as root!"
	exit 1
fi

export SCRIPT_URL="https://gitlab.com/TheNightman/j2-linux/raw/master/installscript.sh"

# Check for dependencies - 7z, mksquashfs, wget, and xorriso
print_status "Checking for dependencies..."
if [ ! -f /usr/bin/xorriso ] || [ ! -f /usr/bin/7z ] || [ ! -f /usr/bin/mksquashfs ] || [ ! -f /usr/bin/wget ]; then
	print_neutral "In order to build a Jaytoo liveCD, you must have 7-zip, wget and squashfs-tools installed. Install them now? (Y/n)"
	read input

	case "$input" in
		y|Y)
			if [ ! -f /usr/bin/xorriso ]; then query="xorriso"; fi
			if [ ! -f /usr/bin/wget ]; then query="$query wget"; fi
			if [ ! -f /usr/bin/mksquashfs ]; then query="$query squashfs-tools"; fi
			if [ ! -f /usr/bin/7z ]; then query="$query p7zip"; fi
			pacman -Syy $(echo "$query") &>> make-iso.out
		;;
		*)	
			print_err "ERR: Missing dependencies. Exiting..."
			exit 1
		;;
	esac
fi

# Download latest debian-live-x.x.x-amd64-xfce-desktop.iso from debian if not already
if [ ! -e install-amd64-minimal-*.iso ]; then
	print_status "Downloading latest gentoo livecd...";
	builddate=$(curl -s http://distfiles.gentoo.org/releases/amd64/autobuilds/current-install-amd64-minimal/ | sed -nr 's/.*href="install-amd64-minimal-([0-9].*).iso">.*/\1/p')
	wget -c "http://distfiles.gentoo.org/releases/amd64/autobuilds/current-install-amd64-minimal/install-amd64-minimal-$builddate.iso" &>> make-iso.out
else
	print_status "Gentoo iso already exists. Continuing...";
fi

export iso=$(ls "$(pwd)"/install-amd64-minimal-* | tail -n1 | sed 's!.*/!!')
mkdir JaytooLive/
export working_dir=JaytooLive

# Extract iso contents
print_status "Extracting iso contents..."
7z x "$iso" -o$working_dir &>> make-iso.out

# Extract the live filesystem
print_status "Decompressing livecd filesystem..."
cd ${working_dir}/
unsquashfs image.squashfs &>> make-iso.out
cd ..

# LiveCD FS is now located at $(pwd)/JaytooLive/live/squashfs-root/

# Grab the installer and put it in place
print_status "Downloading Jaytoo install script and moving into place..."
wget "$SCRIPT_URL" -O ${working_dir}/squashfs-root/usr/bin/Jaytoo &>> make-iso.out
echo '#!/bin/bash' | cat - ${working_dir}/squashfs-root/usr/bin/Jaytoo > temp && mv temp ${working_dir}/squashfs-root/usr/bin/Jaytoo

# Apply permissions
print_status "Making script executable..."
chmod +x ${working_dir}/squashfs-root/usr/bin/Jaytoo &>> make-iso.out

# Remove old live FS
print_status "Removing old squashfs package..."
cd ${working_dir}/
rm -f image.squashfs

# Rebuild the live FS
print_status "Rebuilding filesystem..."
mksquashfs squashfs-root image.squashfs -b 1024k -comp xz &>> make-iso.out
rm -rf squashfs-root

## TODO: Automate replace old md5 in md5sum.txt
print_neutral "$(md5sum image.squashfs)"

cd ..

# Make the ISO
print_status "Building iso..."
xorriso -as mkisofs -r -J \
       	-joliet-long -l -cache-inodes \
       	-isohybrid-mbr /usr/share/syslinux/isohdpfx.bin \
       	-partition_offset 16 -A "Gentoo Live" \
       	-b isolinux/isolinux.bin -c isolinux/boot.cat \
       	-no-emul-boot -boot-load-size 4 -boot-info-table  \
	-o Jaytoo-live-v0.01.iso ${working_dir} &>> make-iso.out

print_neutral "Would you like to clean up (rm JaytooLive)? (y/n)"
read cleanup

case "$cleanup" in
	y|Y)
	    print_status "Cleaning up..."
	    rm -rf ${working_dir}/ &>> make-iso.out
esac

print_status "All set! Iso is located at $(pwd)/Jaytoo-live-v0.01.iso"
