#!/bin/bash
## Chroot methods ((:
source ./.env
install_init() {
    emerge-webrsync
    eselect profile set 16
    echo -e '\nMAKEOPTS="-j2"\nEMERGE_DEFAULT_OPTS="--keep-going=y --autounmask-write=y --jobs=2"\nCFLAGS="-O3 -pipe -march=native"\nCXXFLAGS="\${CFLAGS}"' >> /etc/portage/make.conf
    echo -e '>=sys-apps/util-linux-2.32-r4 static-libs' >> /etc/portage/package.use/util-linux
    if [[ $efi_install = "true" ]]; then
        echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
    fi
}

install_gen_kernel() {
    emerge gcc
    emerge gentoo-sources genkernel btrfs-progs zfs
    wget https://liquorix.net/sources/4.14/config.amd64
    genkernel --kernel-config=config.amd64 all
}

install_bootloader() {
    emerge sys-boot/grub:2
    if [[ $efi_install = "true" ]]; then
        grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
    else
        grub-install $drive
    fi        
    grub-mkconfig -o /boot/grub/grub.cfg
}

install_and_config_packages() {
    emerge xorg-server ${desktop} dhcpcd sddm sudo xfe wpa_supplicant dash porthole firefox vim linux-firmware alsa-utils inconsolata vlgothic liberation-fonts bind-tools colordiff xdg-utils nano
    rm -Rf /usr/portage/packages/*
    sed -i "s/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/" /etc/sudoers
    echo -e "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel\nupdate_config=1" > /etc/wpa_supplicant/wpa_supplicant.conf
}

config_openrc() {
    rc-update add dhcpcd default
    rc-update add alsasound default
    rc-update add wpa_supplicant default
    rc-update add xdm default
}

install_user() {
    echo -e "$rootpassword\n$rootpassword" | passwd
    useradd $user
    echo -e "$userpassword\n$userpassword" | passwd $user
    gpasswd -a $user wheel
    gpasswd -a $user audio
    gpasswd -a $user video
    cd /home/$user/
    mkdir Downloads
    chown -R $user /home/$user/
}

setup_sddm() {
    if [[ -s /etc/conf.d/xdm ]]; then
        echo 'DISPLAYMANAGER="sddm"' >> /etc/conf.d/xdm
    elif [[ ! -f /etc/conf.d/xdm ]]; then
        touch /etc/conf.d/xdm
        echo 'DISPLAYMANAGER="sddm"' >> /etc/conf.d/xdm
    elif [[ -f /etc/conf.d/xdm ]] && [[ ! -s /etc/conf.d/xdm ]]; then
        sed -i 's/DISPLAYMANAGER="xdm"/DISPLAYMANAGER="sddm"/' /etc/conf.d/xdm
    fi
}

install_font() {
    eselect fontconfig enable 52-infinality.conf
    eselect infinality set infinality
    eselect lcdfilter set infinality
}

install_config_locale() {
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
    locale-gen
    eselect locale set en_US.utf8
}