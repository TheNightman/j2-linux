#!/bin/bash

###############################################              
###############################################       
####         
####             -/oyddmdhs+:.                
####         -odNMMMMMMMMNNmhy+-`             
####       -yNMMMMMMMMMMMNNNmmdhy+-           
####     `omMMMMMMMMMMMMNmdmmmmddhhy/`        
####     omMMMMMMMMMMMNh     hmdddhhhdo`      
####    .ydMMMMMMMMMMdh   J   mdddhhhhdm+`    
####     oyhdmNMMMMMMMNh      ddddhhhhyhNd.   
####      :oyhhdNNMMMMMMMNNmmmdddhhhhhyymMh   
####        .:+sydNMMMMMNNNmmmdddhhhhhhmMmy   
####           /mMMMMMMNNNmmmdddhhhhhmMNhs:   
####        `oNMMMMMMMNNNmmmddddhhdmMNhs+`    
####      `sNMMMMMMMMNNNmmmdddddmNMhmhs/.      
####     /NMMMMMMMMNNNNmmmdddmNMNdso:`        
####    +MMMMMMMNNNNmmmmmdmNMNdso/-           
####    yMMNNNNNNNmmmmmNNMmhs+/-`             
####    /hMMNNNNNNNmmNdhs++/-`                
####    `/ohdmmddhys+++/:.`                   
####      `-//////:--. 
####
###############################################              
###############################################

## Make sure we are root user
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
### Print methods and colors(: ###
normal=$'\e[0m'; red=$'\e[31;01m'; green=$'\e[32;01m'; yellow=$'\e[33;01m';

print_status() {
	echo -n -e "${yellow}* ${green}${*}${normal}\n";
}

print_neutral() {
	echo -n -e "${yellow}* ${*}${normal} \n";
}

print_err() {
	echo -n -e "${yellow}* ${red}${*}${normal} \n";
}
### End print methods and colors ###

create_root_passwd() {
    read -sp $print_status "Enter your root password: " rootpassword
    echo
    read -sp $print_status "Confirm root password: " confirmrootpassword
    echo
    if [[ $rootpassword != $confirmrootpassword ]]; then
        print_err "Passwords didn't match"
        create_root_passwd
    fi
    echo "rootpassword=$rootpassword" >> jaytoo/.env
}

create_user() {
    read -e -p $print_status "Enter prefered user name: " -i "" username
    read -e -sp $print_status "Enter user password: " -i "" userpassword
    echo
    read -e -sp $print_status "Confirm user password: " -i "" confirmuserpassword
    echo
    if [[ $userpassword != $confirmuserpassword ]]; then
        print_err "Passwords didn't match! Try again"
        create_user
    fi  
    echo "user=$username" >> jaytoo/.env
    echo "userpass=$userpassword" >> jaytoo/.env
}

check_for_efi() {
    ## Ask if this is an EFI installation
    read -p $print_status "Is this an EFI based install? (Y/n) " -n 1 efi
    case $efi in
        n)  
            efi_install="false"
            echo
            ;;
        *)
            read -e -p $print_status "Enter drive for esp: " -i "/dev/sda1" esppart
            efi_install="true"
            ;;
    esac
}

### Print to the user partition options. We have:
#   ESP: Formatted as FAT32, mounted at /boot/efi ($esppart)
#   Root: Formatted as ext4, mounted at / ($rootpart)
#   Home: Formatted as ext4, mounted at /home/ ($homepart)
### Automatic partitioning will install to a single root partition 
get_partition_info() {
    print_neutral "Automatic (a) partitioning or manual (m) partitioning?"
    read -p "${yellow}  (A/m) -> ${red} " partitioning
    echo "${normal}"
    case $partitioning in
        [aA])
            read -p $print_err "This will wipe your entire hard drive!! Are you sure? [y/n]" -n 1 wipe
            if [[ $wipe != "y" ]] && [[ $wipe != "Y" ]]; then
                exit 1
            fi
            read -erp $print_status "Enter drive for J2 install: " -i "/dev/sda" drive            
            check_for_efi
            read -p "Drive: $drive
            EFI Boot: $efi_install
            Is this correct? [y/n] " -n 1 yn
            if [[ $yn != "y" ]]; then
                exit 1
            fi
            ;;
        *)            
            read -p $print_status "Enter drive for J2 install: (/dev/sda)" -i "/dev/sda" drive
            check_for_efi
            read -e -p $print_status "Enter your root partition: " -i "/dev/sda2" rootpart
            read -p $print_status "Would you like to mount a custom home partition (y/N)" home
            case $home in
                [yY])
                    read -e -p $print_status "Enter your home partition: " -i "/dev/sda3" homepart
                    home_part="true"
                    ;;
            esac
            echo
            print_status "Installation Summary: "
            print_neutral "Drive: ${drive}"
            if [[ $efi_install = "true" ]]; then
                print_neutral "ESP (/boot/efi): $esppart"
            fi
            print_neutral "Root (/): $rootpart"
            if [[ $home_part == "true" ]]; then
                print_neutral "Home (/home): $homepart"
            fi
            read -p "Is this information correct? [y/n]" -n 1 yn
            if [[ $yn != "y" ]] && [[ $yn != "Y" ]]; then
                exit 1
            fi
            ;;
    esac
}

partition_and_mount() {
    mkdir jaytoo
    part=${drive#*/dev/}
    ## Are we Automatically formatting this device???
    if [[ $partitioning = "a" ]] || [[ $partitioning = "A" ]]; then
        print_err "We are about to wipe your drive: ${drive}!"
        print_err "This action cannot be undone!"
        read -e -p $print_status "Are you sure you want to continue? [y/n]" -n 1 cont
        if [[ $cont != "y" ]] && [[ $cont != "Y" ]]; then
            exit 1
        fi
        # Create the partition table with fdisk
        echo -e 'g\nw' | fdisk /dev/$part
        ## Maybe make an efs partition
        if [[ $efi_install = "true" ]]; then
            echo -e "n\n\n\n+550M\nt\n1\nw" | fdisk /dev/$part           
        else
            echo -e "n\n\n\n+1M\nt\n4\nw" | fdisk /dev/$part
            mkdir jaytoo/boot
        fi
        ## Make the root partition
        echo -e "n\n\n\n\nw" | fdisk /dev/$part
        mkfs.ext4 -F /dev/${part}2
        tune2fs -O ^metadata_csum /dev/${part}2
        mount -F /dev/${part}2 jaytoo
        if [[ $efi_install = "true" ]]; then
            mkfs.fat -F 32 /dev/${part}1 
            mkdir -p jaytoo/boot/efi
            mount -F /dev/${part}1 jaytoo/boot/efi
        fi
    else
        mount -F ${rootpart} jaytoo
        if [[ $efi_install = "true" ]]; then
            mkdir -p jaytoo/boot/efi
            mount -F ${esppart} jaytoo/boot/efi
        fi        
        if [[ $home_part = "true" ]]; then
            mkdir jaytoo/home
            mount -F ${homepart} jaytoo/home
        fi
    fi
    get_and_extract_stage3
}

pick_de() {
    desktop="xfce4-meta xfce4-notifyd"
    read -p "The default Desktop Environment is XFCE. Would you like to change that (y/N)?" -n 1 de_change
    case $de_change in
        [Yy])
            echo "Here are the available Desktop environments: \n"
            echo -n -e "${green}* ${green}(X)FCE${normal} \n";
            echo -n -e "${yellow}* ${green}(K)DE Plasma${normal} \n";
            echo -n -e "${yellow}* ${green}(C)innamon${normal} \n";
            echo -n -e "${yellow}* ${green}(LXD)E${normal} \n";
            echo -n -e "${yellow}* ${green}(LXQ)T${normal} \n";
            echo -n -e "${yellow}* ${green}(M)ATE${normal} \n";
            echo 
            echo "Which DE would you like (X|K|C|M|LXD|LXQ)?"
            read DE
            case $DE in        
                K)
                    desktop="plasma-desktop"
                    ;;
                C)
                    desktop="cinnamon"
                    ;;
                M)
                    desktop="mate-base/mate"
                    ;;
                LXD)
                    desktop="lxde-meta"
                    ;;
                LXQ)
                    desktop="lxqt-meta"
                    ;;
                *)
                    desktop="xfce4-meta xfce4-notifyd"
                    ;;
            esac
    esac
    echo "desktop=\"$desktop\"" >> jaytoo/.env
}

get_and_extract_stage3() {
    cd jaytoo/
    builddate=$(curl -s http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/ | sed -nr 's/.*href="stage3-amd64-([0-9].*).tar.xz">.*/\1/p')
	wget -c "http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-$builddate.tar.xz"
    tar pxf stage3*
    rm -f stage3*
    cd ..
}

setup_chroot() {
    cp /etc/resolv.conf jaytoo/etc/
    mount -t proc none jaytoo/proc/
    mount --rbind /dev jaytoo/dev/
    mount --rbind /sys jaytoo/sys/
    wget -c https://gitlab.com/TheNightman/j2-linux/raw/master/chroot.sh
    cp chroot.sh jaytoo/   
}

install() {
cd jaytoo
cat << EOF | chroot .
source chroot.sh
install_init
install_gen_kernel
install_bootloader
install_user
install_and_config_packages
setup_sddm
config_openrc
install_config_locale
install_font
rm .env
rm chroot.sh
exit
EOF
}

get_partition_info
partition_and_mount
pick_de
create_root_passwd
create_user
setup_chroot
install

read -p "Would you like to reboot (y/N)?" re

case $re in
    [yY])
        reboot
	;;
    *)
	exit 0
esac
